var sleep = require('sleep');
var fs = require('fs');
var async = require("async");
var request = require('request');
var service_url = "https://nusoaqa.northwestern.edu/DirectorySearch/res/netid/bas/";



function getMetaData(netid, callback){
	var url = service_url + netid;

	var opts = {
		url: url,
		auth: {
			user: "",
			pass: ""
		}
	};

	request.get(opts, function(err, response, body){
		if (err){
			console.log(err);
		}
		else{
			var responses = JSON.parse(body);
			if (responses.errorCode){
				callback(null, "n/a");
			}
			else{
				console.log(responses);
				callback(null, responses.results[0]);
			}
		}
	})
}


getMetaData('', function (err, data){
	if (err){
		console.log(err);
	}
	else{
		console.log(data);
	}
});